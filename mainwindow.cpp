#include "mainwindow.h"
#include "ui_mainwindow.h"

#include "frmfavloader.h"
#include <QDebug>

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
}

MainWindow::~MainWindow()
{
    delete ui;
}

QStringList MainWindow::tabs()
{
    QStringList mTabNames;
    foreach ( QMdiSubWindow *tab, ui->mdiArea->subWindowList()) {
        qDebug() << tab->windowTitle() << endl;
        mTabNames << tab->windowTitle();
    }
    return mTabNames;
}

void MainWindow::on_actionQuit_triggered()
{
    qApp->quit();
}

void MainWindow::on_actionNew_triggered()
{
    FrmFavLoader *view = new FrmFavLoader;
    ui->mdiArea->addSubWindow(view);
    view->showMaximized();
}

void MainWindow::on_actionSave_triggered()
{
    tabs();
}
