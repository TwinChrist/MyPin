#include "frmfavloader.h"
#include "ui_frmfavloader.h"
#include <QPalette>
#include <QDebug>

FrmFavLoader::FrmFavLoader(QWidget *parent) : QWidget(parent), ui(new Ui::frmFavLoader) {
    ui->setupUi(this);
}

QUrl FrmFavLoader::setUrl(const QString &vUrl) {
    setWindowTitle(vUrl);
    _url.setUrl(vUrl);
    return _url;
}

QUrl FrmFavLoader::setUrl(const QUrl &vUrl) {
    return setUrl(vUrl.url());
}

FrmFavLoader::~FrmFavLoader() {
    delete ui;
}

void FrmFavLoader::on_tbGo_clicked() {
    QString mHttp = "http://";
    QUrl mUrl(ui->leURL->text());
    if(mUrl.isValid()) {
        if(mUrl.scheme().isEmpty()) {
//            mUrl.setScheme("http");
            mUrl.setUrl(mHttp.append(mUrl.url()));
        }
        ui->wevMain->setUrl(setUrl(mUrl));
//        if(ui->leURL->text().isEmpty())
        updateUrlText();
        QPalette mPalette;
        mPalette.setColor(QPalette::Text,Qt::darkGreen);
        ui->leURL->setPalette(mPalette);
    } else {
        QPalette mPalette;
        mPalette.setColor(QPalette::Text,Qt::red);
        ui->leURL->setPalette(mPalette);
    }
    qDebug() << mUrl.url() << endl;
}

void FrmFavLoader::on_wevMain_loadFinished(bool ok)
{
    ui->tbPin->setEnabled(ok);
}

void FrmFavLoader::on_tbPin_clicked()
{
    _pinned = true;
}

void FrmFavLoader::updateUrlText()
{
    ui->leURL->setText(ui->wevMain->page()->url().url());
}

void FrmFavLoader::on_tbBack_clicked()
{
    ui->wevMain->back();
    updateUrlText();
}
