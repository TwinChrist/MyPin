#ifndef FRMFAVLOADER_H
#define FRMFAVLOADER_H

#include <QWidget>
#include <QUrl>

namespace Ui {
class frmFavLoader;
}

class FrmFavLoader : public QWidget
{
    Q_OBJECT

    QUrl _url;
    bool _pinned;
public:
    explicit FrmFavLoader(QWidget *parent = 0);
    QUrl setUrl(const QString &vUrl);
    QUrl setUrl(const QUrl &vUrl);
    ~FrmFavLoader();

private slots:
    void on_tbGo_clicked();
    void on_wevMain_loadFinished(bool);

    void on_tbPin_clicked();
    void updateUrlText();
    void on_tbBack_clicked();

private:
    Ui::frmFavLoader *ui;
};

#endif // FRMFAVLOADER_H
