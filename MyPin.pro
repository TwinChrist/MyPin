#-------------------------------------------------
#
# Project created by QtCreator 2016-08-29T16:10:30
#
#-------------------------------------------------


QT       += core gui webenginewidgets

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = MyPin
TEMPLATE = app

SOURCES += main.cpp\
        mainwindow.cpp \
    frmfavloader.cpp

HEADERS  += mainwindow.h \
    frmfavloader.h

FORMS    += mainwindow.ui \
    frmfavloader.ui

CONFIG += mobility
MOBILITY = 

DISTFILES +=

